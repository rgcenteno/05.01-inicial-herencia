/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package org.daw1.herenciapersona.personal;

/**
 *
 * @author Rafael González Centeno
 */
public class Directivo implements Comparable<Directivo>{
    
    private final String dni;
    private String nombre;
    private CargoDirectivo cargo;

    public Directivo(String dni, String nombre, CargoDirectivo cargo) {
        this.dni = dni;
        this.nombre = nombre;
        this.cargo = cargo;
    }

    /**
     * @return the dni
     */
    public String getDni() {
        return dni;
    }

    /**
     * @return the nombre
     */
    public String getNombre() {
        return nombre;
    }

    /**
     * @param nombre the nombre to set
     */
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    /**
     * @return the cargo
     */
    public CargoDirectivo getCargo() {
        return cargo;
    }

    /**
     * @param cargo the cargo to set
     */
    public void setCargo(CargoDirectivo cargo) {
        this.cargo = cargo;
    }
    
    public void matricular(Alumno a){
        
    }

    @Override
    public String toString() {
        return this.nombre + this.dni + this.cargo;
    }

    @Override
    public boolean equals(Object obj) {
        if(obj == null){
            return false;
        }
        else{
            if(obj instanceof Directivo){
                Directivo aux = (Directivo)obj;
                return this.dni.equals(aux.dni);
            }
            else{
                return false;
            }
        }
    }

    @Override
    public int hashCode() {
        return java.util.Objects.hash(this.nombre, this.dni);
    }

    @Override
    public int compareTo(Directivo t) {
        return (this.nombre + this.dni).compareTo(t.nombre + t.dni);
    }
    
}
