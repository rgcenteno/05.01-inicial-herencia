/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package org.daw1.herenciapersona.personal;

import java.util.Map;
import java.util.TreeMap;

/**
 *
 * @author Rafael González Centeno
 */
public class IES {
    
    private Map<String, Profesor> profesores;
    private Map<String, Alumno> alumnos;
    private Map<String, Directivo> directivos;
    
    private final String nombre;

    public IES(String nombre) {
        this.nombre = nombre;
        this.profesores = new TreeMap<>();
        this.alumnos = new TreeMap<>();
        this.directivos = new TreeMap<>();
    }

    public String getNombre() {
        return nombre;
    }
    
    public String showAllPeopleData(){
        StringBuilder sb = new StringBuilder("Directivos:\n");
        for(Directivo d : directivos.values()){
            
        }
    }
}
